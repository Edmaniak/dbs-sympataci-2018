package controllers;

import models.Employee;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;

public class Secured extends Security.Authenticator {

    public static int ADMIN = 1;
    public static int USER = 2;
    public static int SUPERVIZOR = 3;

    @Override
    public String getUsername(Http.Context ctx) {
        return ctx.session().get("email");
    }

    @Override
    public Result onUnauthorized(Http.Context ctx) {
        return redirect(routes.HomeController.index());
    }

    public static Employee getUser(Http.Context ctx) {
        return Employee.getByEmail(ctx.session().get("email"));
    }

    public static boolean isLoggedIn(Http.Context ctx) {
        return (getUser(ctx) != null);
    }


}
