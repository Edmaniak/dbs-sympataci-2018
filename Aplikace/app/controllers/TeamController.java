package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.*;
import org.h2.engine.User;
import play.data.DynamicForm;
import play.data.Form;
import play.data.FormFactory;
import play.data.validation.ValidationError;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static play.mvc.Controller.flash;
import static play.mvc.Results.badRequest;
import static play.mvc.Results.ok;
import static play.mvc.Results.redirect;

public class TeamController extends Controller {

    @Inject
    FormFactory formFactory;
    Form<Team> teamForm;

    public Result index() {
        if (Secured.getUser(ctx()).getRole().getRoleId() == 1 || Secured.getUser(ctx()).getRole().getRoleId() == 3) {
            List<Team> teams = Team.find.all();
            return ok(views.html.dashboard.team.listing.render("Týmy", teams,Secured.getUser(ctx())));
        }
        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }

    public Result get(Integer id) {
        Employee logged = Secured.getUser(ctx());
        Integer roleId = logged.getRole().getRoleId();
        Team team = Team.find.byId(id);
        ArrayList<Employee> nonTeamEmployees = team.getNonTeamEmployees();
        ArrayList<Employee> teamEmployees = team.getEmployees();
        ArrayList<Project> involvedProjects = Project_Team.getProjects(id);

        if (roleId == 3 && team.getSupervizor().getEmployeeId() == logged.getEmployeeId()) {
            return ok(views.html.dashboard.team.detail.render("Tým " + team.name, team, nonTeamEmployees, teamEmployees, involvedProjects));
        }

        if (logged.getRole().getRoleId() == 1) {
            return ok(views.html.dashboard.team.detail.render("Tým " + team.name, team, nonTeamEmployees, teamEmployees, involvedProjects));
        }

        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }

    public Result delete(Integer id) {
        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            Team t = Team.getById(id);
            Team.delete(id);
            flash("error", t.getName() + " byl vymazán");
            return redirect(routes.TeamController.index());
        }
        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }

    public Result create() {
        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            this.teamForm = formFactory.form(Team.class);
            List<Employee> employees = Employee.find.all();
            ArrayList<Employee> supervisors = Team.getFreeSupervizors();
            if (supervisors.size() == 0) {
                flash("error", "Pro založení nového týmu chybí teamleader, který by ho vedl.");
                return redirect(routes.TeamController.index());
            }
            return ok(views.html.dashboard.team.create.render("Nový tým", teamForm, supervisors, employees));
        }
        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }

    public Result update() {
        Employee logged = Secured.getUser(ctx());
        Integer roleId = logged.getRole().getRoleId();
        DynamicForm form = formFactory.form().bindFromRequest();
        Team team = Team.getById(Integer.valueOf(form.get("team")));
        ArrayList<Employee> freeSupervisors = Team.getFreeSupervizors();
        freeSupervisors.add(team.getSupervizor());

        Employee employee = Employee.getById(Integer.valueOf(form.get("teamleader")));

        team.setName(form.get("name"));
        team.setSupervizor(employee);

        if (team.validate() != null) {
            ArrayList<ValidationError> errors = new ArrayList<>(team.validate());
            String message = "Chyby > ";
            for (ValidationError error : errors)
                message += error.message() + " | ";
            flash("error", message);
            return badRequest(views.html.dashboard.team.update.render("Nový tým", teamForm, team, freeSupervisors));
        }

        if (roleId == 3 && team.getSupervizor().getEmployeeId() == logged.getEmployeeId()) {
            team.edit();
            flash("notification", "Tým " + team.name + " byl editován.");
            return redirect(routes.TeamController.index());
        }

        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            team.edit();
            flash("notification", "Tým " + team.name + " byl editován.");
            return redirect(routes.TeamController.index());
        }

        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());

    }


    public Result edit(Integer id) {
        Employee logged = Secured.getUser(ctx());
        Integer roleId = logged.getRole().getRoleId();
        Team team = Team.getById(id);
        ArrayList<Employee> freeSupervisors = Team.getFreeSupervizors();
        freeSupervisors.add(team.getSupervizor());

        if (roleId == 3 && team.getSupervizor().getEmployeeId() == logged.getEmployeeId()) {
            return ok(views.html.dashboard.team.update.render("Editovat tým " + team.getName(), teamForm, team, freeSupervisors));
        }

        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            return ok(views.html.dashboard.team.update.render("Editovat tým " + team.getName(), teamForm, team, freeSupervisors));
        }

        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }

    public Result addTeamMember() {
        Employee logged = Secured.getUser(ctx());
        Integer roleId = logged.getRole().getRoleId();
        DynamicForm form = formFactory.form().bindFromRequest();
        Integer employeeId = Integer.valueOf(form.get("employee"));
        Integer teamId = Integer.valueOf(form.get("team"));
        Employees_Teams allocation = new Employees_Teams(employeeId, teamId);

        if (roleId == 3 && Team.getById(teamId).getSupervizor().getEmployeeId() == logged.getEmployeeId()) {
            allocation.insert();
            flash("notification", Employee.getById(employeeId) + " byl přidán do týmu " + Team.getById(teamId));
            return redirect(routes.TeamController.get(Integer.valueOf(form.get("team"))));
        }

        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            allocation.insert();
            flash("notification", Employee.getById(employeeId) + " byl přidán do týmu " + Team.getById(teamId));
            return redirect(routes.TeamController.get(Integer.valueOf(form.get("team"))));
        }

        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());

    }

    public Result removeTeamMember(Integer teamId, Integer employeId) {
        Employee logged = Secured.getUser(ctx());
        Integer roleId = logged.getRole().getRoleId();
        Team team = Team.getById(teamId);
        Employee employee = Employee.getById(employeId);
        if (team.getSupervizor().getEmployeeId() == employeId) {
            flash("error", employee.getFirstname() + " " + employee.getLastname() + " je supervizor týmu " + team.getName() + "a nemůže být odebrán jinak než editací týmu");
            return redirect(routes.TeamController.get(teamId));
        }

        if (roleId == 3 && Team.getById(teamId).getSupervizor().getEmployeeId() == logged.getEmployeeId()) {
            team.removeTeamMember(employeId);
            flash("notification", employee.getFirstname() + " " + employee.getLastname() + " byl odebrán z týmu " + team.getName());
            return redirect(routes.TeamController.get(teamId));
        }

        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            team.removeTeamMember(employeId);
            flash("notification", employee.getFirstname() + " " + employee.getLastname() + " byl odebrán z týmu " + team.getName());
            return redirect(routes.TeamController.get(teamId));
        }

        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }

    public Result save() {
        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            List<Employee> employees = Employee.find.all();
            ArrayList<Employee> supervisors = Team.getFreeSupervizors();
            DynamicForm form = formFactory.form().bindFromRequest();
            String name = form.get("title");
            Employee teamleader = Employee.getById(Integer.parseInt(form.get("teamleader")));
            Team team = new Team(name, teamleader);

            if (team.validate() != null) {
                ArrayList<ValidationError> errors = new ArrayList<>(team.validate());
                String message = "Chyby > ";
                for (ValidationError error : errors)
                    message += error.message() + " | ";
                flash("error", message);
                return badRequest(views.html.dashboard.team.create.render("Nový tým", teamForm, supervisors, employees));
            }

            team.insert();
            flash("notification", "Tým " + team.name + " vytvořen");
            return redirect(routes.TeamController.index());
        }
        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }
}
