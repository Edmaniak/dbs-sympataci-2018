package controllers;

import models.Assigment;
import models.Employee;
import models.Project;
import models.Task;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;

import java.util.ArrayList;

import static play.mvc.Controller.request;
import static play.mvc.Results.ok;
import static play.mvc.Results.redirect;

public class AssigmentController extends Controller {

    @Inject
    FormFactory formFactory;

    public Result get(Integer id) {
        Employee logged = Secured.getUser(ctx());
        Assigment assigment = Assigment.getById(id);
        Project project = assigment.getProject();
        ArrayList<Task> tasks = new ArrayList<>(assigment.getTasks());
        if (logged.getRole().getRoleId() == 3 && project.canBeSupervisedBy(logged)) {
            return ok(views.html.dashboard.assigment.detail.render(assigment.title, project, tasks));
        }
        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            return ok(views.html.dashboard.assigment.detail.render(assigment.title, project, tasks));
        }
        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }

    public Result delete(Integer id) {
        Assigment a = Assigment.getById(id);
        Project project = a.getProject();
        Employee logged = Secured.getUser(ctx());
        if (logged.getRole().getRoleId() == 3 && project.canBeSupervisedBy(logged)) {
            Assigment.delete(id);
            flash("notification", a.getTitle() + " byl vymazán z projektu " + project.getName());
            return redirect(routes.ProjectController.get(a.getProject().getProjectId()));
        }

        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            Assigment.delete(id);
            flash("notification", a.getTitle() + " byl vymazán z projektu " + project.getName());
            return redirect(routes.ProjectController.get(a.getProject().getProjectId()));
        }

        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }


}
