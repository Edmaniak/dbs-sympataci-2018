package controllers;

import models.*;
import org.joda.time.format.DateTimeFormat;
import play.data.DynamicForm;
import play.data.Form;
import play.data.FormFactory;
import play.data.validation.ValidationError;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import javax.xml.bind.ValidationEvent;
import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import java.util.List;

public class ProjectController extends Controller {

    @Inject
    FormFactory formFactory;
    Form<Project> projectForm;

    public Result addAssigment() {
        Employee logged = Secured.getUser(ctx());
        DynamicForm formAssigment = formFactory.form().bindFromRequest();
        ArrayList<ValidationError> errors = new ArrayList<>();
        Integer projectId = Integer.valueOf(formAssigment.get("project"));
        Integer teamId = Integer.valueOf(formAssigment.get("team"));
        Project project = Project.getById(projectId);
        Team team = Team.getById(teamId);
        String title = formAssigment.get("title");
        String description = formAssigment.get("description");
        Assigment assigment = new Assigment(title, description, project, team);
        if (assigment.validate() != null)
            errors.addAll(assigment.validate());
        if (errors.size() > 0) {
            String message = "Chyby > ";
            for (ValidationError error : errors)
                message += error.message() + " | ";
            flash("error", message);
            return get(projectId);
        }

        if (logged.getRole().getRoleId() == 3 && project.canBeSupervisedBy(logged)) {
            assigment.insert();
            flash("notification", assigment.getTitle() + " byl alokován k projektu " + project.getName());
            return redirect(routes.ProjectController.get(project.projectId));
        }

        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            assigment.insert();
            flash("notification", assigment.getTitle() + " byl alokován k projektu " + project.getName());
            return redirect(routes.ProjectController.get(project.projectId));
        }
        
        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }

    public Result addTask() {
        Employee logged = Secured.getUser(ctx());
        DynamicForm formTask = formFactory.form().bindFromRequest();
        ArrayList<ValidationError> errors = new ArrayList<>();

        Integer assigmentId = Integer.valueOf(formTask.get("assigment"));
        Assigment assigment = Assigment.getById(assigmentId);
        Project p = assigment.getProject();
        Integer employeeId = Integer.valueOf(formTask.get("employee"));
        Employee employee = Employee.getById(employeeId);
        String subject = formTask.get("subject");
        String description = formTask.get("description");
        String dateS = formTask.get("date");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate date = LocalDate.parse(dateS, formatter);
        Integer hours = 0;

        try {
            hours = Integer.valueOf(formTask.get("hours"));
        } catch (Exception e) {
            errors.add(new ValidationError("hours", "Počet hodin musí být číslo"));
        }

        Task task = new Task(date, hours, subject, description, assigment, employee);
        if (task.validate() != null)
            errors.addAll(task.validate());
        if (errors.size() > 0) {
            String message = "Chyby > ";
            for (ValidationError error : errors)
                message += error.message() + " | ";
            flash("error", message);
            return redirect(routes.ProjectController.get(p.getProjectId()));
        }

        if (logged.getRole().getRoleId() == 3 && p.canBeSupervisedBy(logged)) {
            task.insert();
            flash("notification", "Pracovní záznam " + subject + " byl přidán.");
            return redirect(routes.ProjectController.get(p.getProjectId()));
        }

        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            task.insert();
            flash("notification", "Pracovní záznam " + subject + " byl přidán.");
            return redirect(routes.ProjectController.get(p.getProjectId()));

        }

        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }

    public Result removeTeam(Integer projectId, Integer teamId) {
        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            Project project = Project.getById(projectId);
            project.removeTeam(teamId);
            flash("notification", "Tým " + Team.getById(teamId).getName() + " byl odebrán.");
            return redirect(routes.ProjectController.get(projectId));
        }
        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }

    public Result addTeam() {
        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            DynamicForm form = formFactory.form().bindFromRequest();
            Integer projectId = Integer.valueOf(form.get("project"));
            Integer teamId = Integer.valueOf(form.get("team"));
            Project_Team allocation = new Project_Team(projectId, teamId);
            allocation.insert();
            flash("notification", "Tým " + Team.getById(teamId).getName() + " byl alokován.");
            return redirect(routes.ProjectController.get(projectId));
        }
        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }

    public Result addCost() {
        DynamicForm formCost = formFactory.form().bindFromRequest();
        ArrayList<ValidationError> errors = new ArrayList<>();
        Integer projectId = Integer.valueOf(formCost.get("project"));
        Project project = Project.getById(projectId);
        Integer cost = 0;
        Employee logged = Secured.getUser(ctx());

        try {
            cost = Integer.valueOf(formCost.get("cost"));
        } catch (Exception ex) {
            errors.add(new ValidationError("cost", "Částka musí být celé číslo"));
        }

        String name = formCost.get("name");
        String description = formCost.get("describtion");
        Cost cost1 = new Cost(name, description, cost, project);

        if (cost1.validate() != null)
            errors.addAll(cost1.validate());

        if (errors.size() > 0) {
            String message = "Chyby > ";
            for (ValidationError error : errors)
                message += error.message() + " | ";
            flash("error", message);
            return get(projectId);
        }

        if (logged.getRole().getRoleId() == 3 && project.canBeSupervisedBy(logged)) {
            cost1.insert();
            flash("notification", "Výdaj byl přidán k projektu" + project.getName());
            return redirect(routes.ProjectController.get(project.projectId));
        }

        if (logged.getRole().getRoleId() == 1) {
            cost1.insert();
            flash("notification", "Výdaj byl přidán k projektu" + project.getName());
            return redirect(routes.ProjectController.get(project.projectId));
        }

        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }


    public Result index() {
        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            List<Project> projects = Project.find.all();
            return ok(views.html.dashboard.project.listing.render("Projekty", projects));
        }

        if(Secured.getUser(ctx()).getRole().getRoleId() == 3) {
            List<Project> projects = Secured.getUser(ctx()).getAllSupervisedProjects();
            return ok(views.html.dashboard.project.listing.render("Projekty", projects));
        }
        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }

    public Result get(Integer id) {
        Project p = Project.find.byId(id);
        Form<Assigment> assigmentForm = formFactory.form(Assigment.class);
        ArrayList<Team> teams = Team.getAvailableTeamsForProject(p);
        ArrayList<Assigment> assigments = new ArrayList<>(p.getAssigment());
        ArrayList<Employee> involvedEmployees = p.getAllInvolvedEmployees();
        ArrayList<Team> involvedTeams = Project_Team.getTeams(id);
        ArrayList<Task> doneTasks = p.getAllTasks();
        Form<Task> taskForm = formFactory.form(Task.class);

        Employee logged = Secured.getUser(ctx());

        if (logged.getRole().getRoleId() == 3 && p.canBeSupervisedBy(logged)) {
            return ok(views.html.dashboard.project.detail.render("Projekt " + p.name, p, assigmentForm, teams, assigments, involvedTeams, involvedEmployees, doneTasks, taskForm, logged));
        }

        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            return ok(views.html.dashboard.project.detail.render("Projekt " + p.name, p, assigmentForm, teams, assigments, involvedTeams, involvedEmployees, doneTasks, taskForm,logged));
        }

        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }


    public Result create() {
        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            this.projectForm = formFactory.form(Project.class);
            ArrayList<Customer> customers = Customer.getAll();
            if (customers.size() == 0) {
                flash("error", "Nelze vytvořit projekt - v databázi není zákazník. Zde ho můžete vytvořit");
                return redirect(routes.CustomerController.create());
            }
            return ok(views.html.dashboard.project.create.render("Nový projekt", projectForm, customers));
        }
        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }

    public Result update() {
        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            DynamicForm form = formFactory.form().bindFromRequest();
            Project project = Project.getById(Integer.valueOf(form.get("projectId")));
            project.setBudget(Integer.valueOf(form.get("budget")));
            project.setName(form.get("name"));
            //TODO project.setDeadLine();
            project.setCustomer(Customer.getById(Integer.valueOf(form.get("customer"))));
            project.edit();
            flash("notification", project.name + " byl úspěšně editován");
            return redirect(routes.ProjectController.index());
        }
        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }

    public Result edit(Integer id) {
        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            this.projectForm = formFactory.form(Project.class);
            Project project = Project.getById(id);
            ArrayList<Customer> customers = Customer.getAll();
            return ok(views.html.dashboard.project.update.render("Editace projektu " + project.getName(), projectForm, customers, project));
        }
        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }

    public Result delete(Integer id) {
        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            Project p = Project.getById(id);
            Project.delete(id);
            flash("error", p.name + "byl smazán");
            return redirect(routes.ProjectController.index());
        }
        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }

    public Result changeState(Integer id) {
        Employee logged = Secured.getUser(ctx());
        Project project = Project.getById(id);
        project.setState(new Boolean(!project.getState()));

        if (logged.getRole().getRoleId() == 3 && project.canBeSupervisedBy(logged)) {
            project.edit();
            flash("notification", "Status projektu byl změněn");
            return redirect(request().getHeader("referer"));
        }

        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            project.edit();
            flash("notification", "Status projektu byl změněn");
            return redirect(request().getHeader("referer"));
        }

        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }

    public Result save() {
        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            DynamicForm form = formFactory.form().bindFromRequest();
            String name = form.get("name");
            String budget = form.get("budget");
            String deadline = form.get("deadline");
            Customer c = Customer.getById(Integer.parseInt(form.get("customer")));
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
            LocalDate date = LocalDate.parse(deadline, formatter);
            Project pr = new Project(name, Integer.parseInt(budget), date, c);
            pr.insert();
            flash("notification", "Projekt " + pr.name + " vytvořen");
            return redirect(routes.ProjectController.index());
        }
        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }

}
