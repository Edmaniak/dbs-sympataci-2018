package controllers;

import models.Employee;
import models.Project;
import models.Task;
import play.mvc.Controller;
import play.mvc.Result;

public class TaskController extends Controller {

    public Result delete(Integer id) {
        Employee logged = Secured.getUser(ctx());
        Task task = Task.getById(id);

        Project project = task.getAssigment().getProject();

        if (logged.getRole().getRoleId() == 3 && project.canBeSupervisedBy(logged)) {
            Task.delete(id);
            flash("notification", task.subject + " byl vymazán");
            return redirect(request().getHeader("referer"));
        }

        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            Task.delete(id);
            flash("notification", task.subject + " byl vymazán");
            return redirect(request().getHeader("referer"));
        }

        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }


}
