package controllers;

import models.*;
import play.data.DynamicForm;
import play.data.Form;
import play.data.FormFactory;
import play.data.validation.ValidationError;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static play.mvc.Controller.flash;
import static play.mvc.Results.ok;
import static play.mvc.Results.redirect;

public class FinancesController extends Controller {

    @Inject
    FormFactory formFactory;

    public Result index() {
        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            ArrayList<Cost> costs = Cost.getAll();
            ArrayList<Project> projects = Project.getAll();
            Integer totalCost = Cost.getAllCosts();
            return ok(views.html.dashboard.finances.listing.render("Finance", costs, projects, totalCost));
        }
        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }


    public Result get(Integer id) {
        return ok();
    }

    public Result create() {
        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            Form<Cost> form = formFactory.form(Cost.class);
            ArrayList<Project> projects = Project.getAll();
            return ok(views.html.dashboard.finances.create.render("Přidat výdaj", form, projects, null));
        }
        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }

    public Result createToProject(Integer projectId) {
        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            Form<Cost> form = formFactory.form(Cost.class);
            ArrayList<Project> projects = new ArrayList<>();
            Project project = Project.getById(projectId);
            return ok(views.html.dashboard.finances.create.render("Přidat výdaj pro projekt " + project.name, form, projects, project));
        }
        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());

    }

    public Result delete(Integer id) {
        return ok();
    }

    public Result update() {
        return ok();
    }

    public Result edit(Integer id) {
        return ok();
    }

    public Result save() {
        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            DynamicForm form = formFactory.form().bindFromRequest();
            ArrayList<ValidationError> errors = new ArrayList<>();

            String name = form.get("name");
            String description = form.get("describtion");
            Integer cost = 0;
            try {
                cost = Integer.valueOf(form.get("cost"));
            } catch (Exception e) {
                errors.add(new ValidationError("cost","Částka musí být celé číslo"));
            }

            Project project = Project.getById(Integer.valueOf(form.get("project")));
            Cost costObject = new Cost(name, description, cost, project);

            if (costObject.validate() != null)
                errors.addAll(costObject.validate());

            if (errors.size() > 0) {
                String message = "Chyby > ";
                for (ValidationError error : errors)
                    message += error.message() + " | ";
                flash("error", message);
                return create();
            }
            costObject.insert();
            flash("notification", "Výdaj " + costObject.name + " byl vytvořen pro projekt " + project.getName());
            return redirect(routes.FinancesController.index());
        }
        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }


    public Result getFinancesOfProject(Integer projectId) {
        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            Project project = Project.getById(projectId);
            List<Cost> costs = new ArrayList<>(project.getAllCosts());
            Integer totalCost = project.getOtherCostsAmount();
            ArrayList<Project> projects = Project.getAll();
            return ok(views.html.dashboard.finances.listing.render("Finance pro projekt " + project.name, costs, projects, totalCost));
        }
        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }
}
