package controllers;

import models.*;

import org.apache.commons.io.FileUtils;
import play.data.DynamicForm;
import play.data.Form;
import play.data.FormFactory;
import play.data.validation.ValidationError;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;
import views.formdata.LoginFormData;
import views.html.login;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class UserController extends Controller {

    @Inject
    FormFactory formFactory;


    public Result index() {
        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            ArrayList<Employee> employees = Employee.getAll();
            return ok(views.html.dashboard.employee.listing.render("Zaměstnanci", employees));
        }
        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }

    public Result get(Integer id) {
        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            Employee employee = Employee.getById(id);
            ArrayList<Assigment> assigments = employee.getAllAssigments();
            ArrayList<Team> teams = employee.getAllTeams();
            Form<Task> taskForm = formFactory.form(Task.class);
            return ok(views.html.dashboard.employee.detail.render(employee.firstname + " " + employee.lastname, employee, assigments, teams, taskForm));
        }
        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }

    public Result getTasksOfAssigment(Integer id, Integer idAssigment) {
        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            Employee employee = Employee.getById(id);
            Assigment assigment = Assigment.getById(idAssigment);
            ArrayList<Task> tasks = employee.getTasksOfAssigment(idAssigment);
            ArrayList<Team> teams = employee.getAllTeams();
            Form<Task> taskForm = formFactory.form(Task.class);
            String title = "Práce zaměstnance " + employee.getLastname() + " " + " na úkolu " + assigment.getTitle();
            return ok(views.html.dashboard.employee.assigment.render(title, employee, tasks, teams, assigment, taskForm));
        }
        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }

    public Result tasks(Integer id) {
        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            Employee employee = Employee.getById(id);
            ArrayList<Task> tasks = new ArrayList<>(employee.getTasks());
            ArrayList<Team> teams = employee.getAllTeams();
            return ok(views.html.dashboard.employee.tasks.render("Všechny práce pracovníka " + employee.getLastname(), employee, tasks, teams));
        }
        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }

    public Result create() {
        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            Form<Employee> employeeForm = formFactory.form(Employee.class);
            Form<Address> addressForm = formFactory.form(Address.class);
            ArrayList<Role> roles = Role.getAll();
            return ok(views.html.dashboard.employee.create.render("Přidat zaměstnance", employeeForm, addressForm, roles));
        }
        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }

    public Result delete(Integer id) {
        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            Employee employee = Employee.getById(id);
            Employee.delete(id);
            flash("error", "Zaměstnanec " + employee.getLastname() + " byl smazán");
            return redirect(routes.UserController.index());
        }
        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }

    public Result update() {
        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            DynamicForm form = formFactory.form().bindFromRequest();
            Employee employee = Employee.getById(Integer.valueOf(form.get("employeeId")));
            Address address = Address.getById(Integer.valueOf(form.get("addressId")));
            ArrayList<ValidationError> errors = new ArrayList<>();
            String password = form.get("password");
            String passwordAgain = form.get("passwordAgain");
            if (!password.equals(passwordAgain))
                errors.add(new ValidationError("password", "Hesla se neshodují"));
            Double tax = 0d;
            try {
                tax = Double.valueOf(form.get("tax"));
            } catch (Exception e) {
                errors.add(new ValidationError("tax", "Základní taxa musí být číslo"));
            }

            employee.setFirstname(form.get("firstname"));
            employee.setLastname(form.get("lastname"));
            employee.setPassword(form.get("password"));
            employee.setTax(tax);
            employee.setRole(Role.getById(Integer.valueOf(form.get("role"))));
            address.setTown(form.get("town"));
            address.setTelnumber(form.get("telnumber"));
            address.setStreet(form.get("street"));
            address.setEmail(form.get("email"));
            address.setCitycode(form.get("citycode"));

            if (address.validate() != null)
                errors.addAll(address.validate());
            if (employee.validate() != null)
                errors.addAll(employee.validate());

            if (errors.size() > 0) {
                String message = "Chyby > ";
                for (ValidationError error : errors)
                    message += error.message() + " | ";
                flash("error", message);
                return create();
            }

            employee.edit();
            address.edit();
            flash("notification", "Zaměstnanec " + employee.getLastname() + " byl úspěšně editován");
            return redirect(routes.UserController.index());
        }
        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }

    public Result edit(Integer id) {
        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            Form<Employee> employeeForm = formFactory.form(Employee.class);
            Form<Address> addressForm = formFactory.form(Address.class);
            Employee employee = Employee.getById(id);
            ArrayList<Role> roles = Role.getAll();
            String name = employee.getFirstname() + " " + employee.getLastname();
            return ok(views.html.dashboard.employee.update.render("Editovat zaměstnance " + name, employeeForm, addressForm, roles, employee));
        }
        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }

    public Result save() {
        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {


            DynamicForm form = formFactory.form().bindFromRequest();
            ArrayList<ValidationError> errors = new ArrayList<>();

            String password = form.get("password");
            String passwordAgain = form.get("passwordAgain");
            if (!password.equals(passwordAgain))
                errors.add(new ValidationError("password", "Hesla se neshodují"));

            String firstname = form.get("firstname");
            String lastname = form.get("lastname");
            Double tax = 0d;
            try {
                tax = Double.valueOf(form.get("tax"));
            } catch (Exception e) {
                errors.add(new ValidationError("tax", "Základní taxa musí být číslo"));
            }

            String email = form.get("email");
            String street = form.get("street");
            String citycode = form.get("citycode");
            String town = form.get("town");
            String telnumber = form.get("telnumber");

            Address address = new Address(street, citycode, telnumber, town, email);
            Integer roleId = Integer.valueOf(form.get("role"));
            Role role = Role.getById(roleId);
            Http.MultipartFormData<File> body = request().body().asMultipartFormData();
            Http.MultipartFormData.FilePart<File> photo = body.getFile("photo");
            Photo photoModel = new Photo(photo.getContentType(), "images/photo/" + photo.getFilename(), 10d);

            Employee employee = new Employee(firstname, lastname, password, Double.valueOf(tax), address, role, photoModel);

            if (address.validate() != null)
                errors.addAll(address.validate());
            if (employee.validate() != null)
                errors.addAll(employee.validate());

            if (photo.getFilename().length() > 0) {
                String fileName = photo.getFilename();
                String contentType = photo.getContentType();
                File file = photo.getFile();
                try {
                    FileUtils.moveFile(file, new File("public/images/photo", fileName));
                } catch (IOException ioe) {
                    errors.add(new ValidationError("error", "Nepodařilo se nahrát fotku"));
                }
            }

            if (errors.size() > 0) {
                String message = "Chyby > ";
                for (ValidationError error : errors)
                    message += error.message() + " | ";
                flash("error", message);
                return create();
            }

            photoModel.save();
            address.insert();
            employee.insert();

            flash("notification", "Zaměstnanec " + employee.getFirstname() + " " + employee.getLastname() + " byl vytvořen");
            return redirect(routes.UserController.index());
        }
        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }

    @Security.Authenticated(Secured.class)
    public Result logout() {
        session().clear();
        return redirect(routes.HomeController.index());
    }

    public Result login() {
        Form<LoginFormData> form = formFactory.form(LoginFormData.class).bindFromRequest();
        if (form.hasErrors()) {
            flash("error", "Přihlašovací údaje nejsou správně vyplněny");
            return badRequest(login.render("Project-manager - přihlášení", form));
        } else {
            session().clear();
            session("email", form.get().email);
            flash("notification", "Byl/a jste úspěšně přihlášena");
            return redirect(routes.HomeController.dashboard());
        }

    }


}
