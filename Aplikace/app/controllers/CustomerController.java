package controllers;

import models.*;
import play.data.DynamicForm;
import play.data.Form;
import play.data.FormFactory;
import play.data.validation.ValidationError;
import play.mvc.Controller;
import play.mvc.Result;
import scala.Int;

import javax.inject.Inject;
import java.util.ArrayList;

public class CustomerController extends Controller {

    @Inject
    FormFactory formFactory;

    public Result index() {
        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            ArrayList<Customer> customers = Customer.getAll();
            return ok(views.html.dashboard.customer.listing.render("Zákazníci", customers));
        }
        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }

    public Result get(Integer id) {
        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            Customer c = Customer.getById(id);
            Form<Project> projectForm = formFactory.form(Project.class);
            ArrayList<Project> projects = new ArrayList<>(c.getProjects());
            return ok(views.html.dashboard.customer.detail.render(c.name, c, projectForm, projects));
        }
        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }

    public Result create() {
        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            Form<Customer> customerForm = formFactory.form(Customer.class);
            Form<Address> addressForm = formFactory.form(Address.class);
            return ok(views.html.dashboard.customer.create.render("Nový zákazník", customerForm, addressForm));
        }
        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }

    public Result delete(Integer id) {
        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            Customer.delete(id);
            flash("alert", "Zákazník " + "byl smazán");
            return redirect(routes.CustomerController.index());
        }
        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }

    public Result update() {
        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            DynamicForm form = formFactory.form().bindFromRequest();
            Integer addressId = Integer.valueOf(form.get("addressId"));
            Integer customerId = Integer.valueOf(form.get("customerId"));
            Customer customer = Customer.getById(customerId);
            Address address = Address.getById(addressId);

            address.setCitycode(form.get("citycode"));
            address.setEmail(form.get("email"));
            address.setStreet(form.get("street"));
            address.setTelnumber(form.get("telnumber"));
            address.setTown(form.get("town"));
            customer.setName(form.get("name"));

            if (address.validate() != null || customer.validate() != null) {
                ArrayList<ValidationError> errors = new ArrayList<>(address.validate());
                if (customer.validate() != null)
                    errors.addAll(customer.validate());
                String message = "Chyby > ";
                for (ValidationError error : errors)
                    message += error.message() + " | ";
                flash("error", message);
                return edit(customerId);
            }

            customer.edit();
            address.edit();
            flash("notification", "Zákazník " + customer.getName() + " byl úspěšně smazán editován");
            return redirect(routes.CustomerController.index());
        }
        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }

    public Result edit(Integer id) {
        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            Form<Customer> customerForm = formFactory.form(Customer.class);
            Form<Address> addressForm = formFactory.form(Address.class);
            Customer customer = Customer.getById(id);
            return ok(views.html.dashboard.customer.update.render("Upravit " + customer.getName(), customer, customerForm, addressForm));
        }
        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }

    public Result save() {
        if (Secured.getUser(ctx()).getRole().getRoleId() == 1) {
            DynamicForm form = formFactory.form().bindFromRequest();

            String email = form.get("email");
            String street = form.get("street");
            String citycode = form.get("citycode");
            String town = form.get("town");
            String telnumber = form.get("telnumber");

            Address address = new Address(street, citycode, town, telnumber, email);
            Customer customer = new Customer(form.get("name"), address);
            if (address.validate() != null || customer.validate() != null) {
                ArrayList<ValidationError> errors = new ArrayList<>(address.validate());
                if (customer.validate() != null)
                    errors.addAll(customer.validate());
                String message = "Chyby > ";
                for (ValidationError error : errors)
                    message += error.message() + " | ";
                flash("error", message);
                return create();
            }

            address.insert();
            customer.insert();

            flash("notification", "Zákazník " + customer.name + " byl vytvořen");
            return redirect(routes.CustomerController.index());
        }
        flash("error", "K tomuto nemáte oprávnění");
        return redirect(routes.HomeController.profile());
    }


}
