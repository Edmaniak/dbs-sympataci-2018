package controllers;

import models.*;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.*;

import play.twirl.api.Content;
import play.twirl.api.Html;
import views.formdata.LoginFormData;
import views.html.*;

import javax.inject.Inject;
import java.util.ArrayList;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {

    private ProjectController projectController;
    @Inject
    FormFactory formFactory;

    public HomeController() {
        projectController = new ProjectController();
    }

    public Result index() {
        Form<LoginFormData> formData = formFactory.form(LoginFormData.class);
        return ok(login.render("Project-manager - přihlášení", formData));
    }

    @Security.Authenticated(Secured.class)
    public Result dashboard() {
        Employee loggedUser = Secured.getUser(ctx());
        if (loggedUser.getRole().getRoleId() == Secured.ADMIN)
            return projectController.index();
        return profile();
    }

    @Security.Authenticated(Secured.class)
    public Result profile() {
        Employee loggedUser = Secured.getUser(ctx());
        System.out.println(loggedUser);
        ArrayList<Team> teams = loggedUser.getAllTeams();
        System.out.println(teams);
        ArrayList<Assigment> assigments = loggedUser.getAllAssigments();
        Form<Task> taskForm = formFactory.form(Task.class);
        return ok(views.html.dashboard.profile.profile.render("Osobní profil", loggedUser, teams, assigments, taskForm));
    }

    @Security.Authenticated(Secured.class)
    public Result tasks() {
        Employee loggedUser = Secured.getUser(ctx());
        ArrayList<Team> teams = loggedUser.getAllTeams();
        ArrayList<Task> tasks = new ArrayList<>(loggedUser.getTasks());
        return ok(views.html.dashboard.profile.tasks.render("Zaznamenané práce", loggedUser, tasks,teams));
    }

    @Security.Authenticated(Secured.class)
    public Result getAssigmentTasks(Integer assigmentId) {
        Employee loggedUser = Secured.getUser(ctx());
        Assigment assigment = Assigment.getById(assigmentId);
        ArrayList<Task> tasks = new ArrayList<>(assigment.getTasks());
        ArrayList<Team> teams = loggedUser.getAllTeams();
        Form<Task> taskForm = formFactory.form(Task.class);
        return ok(views.html.dashboard.profile.assigment.render("Práce na úkolu " + assigment.title, loggedUser, tasks, teams, assigment, taskForm));
    }


}
