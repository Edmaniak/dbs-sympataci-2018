package models;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import io.ebean.Ebean;
import io.ebean.Finder;
import io.ebean.Model;

@Entity
@Table(name = "project")
public class Project extends Model {

    @Id
    public Integer projectId;
    public String name;
    public Integer budget;
    public Date deadLine;
    // pracuje se - true / hotovo - false
    public Boolean state;

    @ManyToOne(optional = true)
    public Customer customer;

    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL)
    public List<Cost> costs;


	public static Project getById(int id) {
		return null;
	}


    public Project(String name, Integer budget, LocalDate deadLine, Customer customer) {
        this.name = name;
        this.budget = budget;
        this.deadLine = Date.from(deadLine.atStartOfDay(ZoneId.systemDefault()).toInstant());
        this.customer = customer;
        this.state = true;
    }

    @Override
    public String toString() {
        return name + " " + budget;
    }

    public static Project getById(int id) {
        return find.byId(id);
    }

    public static ArrayList<Project> getAll() {
        return new ArrayList<>(find.all());
    }

    @Override
    public void insert() {
        this.save();
    }


    public void edit() {
        this.update();
    }
//TODO
    }


    public List<Cost> getAllCosts() {
        List<Cost> costs = Ebean.find(Cost.class).where().eq("project_project_id", this.projectId).findList();
        return costs;
    }

    public Integer getOtherCostsAmount() {
        Integer costs = 0;
        for (Cost cost : getAllCosts()) {
            costs += cost.getCost();
        }
        return costs;
    }

    public int getEmployeeCosts() {
        Double amount = 0d;
        for (Task task : getAllTasks()) {
            amount += (task.getHours() * task.getEmployee().getTax());
        }
        return (int) Math.round(amount);
    }

    public List<Team> getAllInvolvedTeams() {
        List<Team> teams = Ebean.find(Team.class).fetch("team").fetch("assigment").where().eq("project_project_id", this.projectId).findList();
        return teams;
    }

    public ArrayList<Employee> getAllInvolvedEmployees() {
        ArrayList<Employee> employees = new ArrayList<>();
        for (Team t : getAllInvolvedTeams()) {
            for (Employee e : t.getEmployees())
                employees.add(e);
            employees.add(t.getSupervizor());
        }
        return employees;
    }

    public Integer getProggres() {
        Integer proggress = 0;
        for (Task task : getAllTasks()) {
            proggress += task.getHours();
        }
        return proggress;
    }

    public boolean canBeSupervisedBy(Employee employee) {
        for (Team t : getAllInvolvedTeams())
            if (t.getSupervizor().getEmployeeId() == employee.getEmployeeId())
                return true;
        return false;
    }

    public List<Assigment> getAssigment() {
        return assigment;
    }

    public ArrayList<Task> getAllTasks() {
        ArrayList<Task> tasky = new ArrayList<>();
        for (Assigment assigment : getAssigment()) {
            ArrayList<Task> tasks = new ArrayList<>(assigment.getTasks());
            for (Task task : tasks) {
                tasky.add(task);
            }
        }
        return tasky;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public Customer getCustomer() {
        return customer;
    }

    public String getName() {
        return name;
    }

    public Boolean getState() {
        return this.state;
    }

    public String getDeadLine() {
        SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
        return f.format(deadLine);
    }

    public Integer getBudget() {
        return budget;
    }

    public List<Cost> getCosts() {
        return costs;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBudget(Integer budget) {
        this.budget = budget;
    }

    public void setDeadLine(Date deadLine) {
        this.deadLine = deadLine;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
