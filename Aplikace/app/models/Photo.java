package models;

import javax.persistence.*;

import io.ebean.Finder;
import io.ebean.Model;

import java.util.ArrayList;


@Entity
@Table(name="photo")
public class Photo extends Model {

	@Id
	public Integer photoId;
	public String format;
	public String name;
	public Double size;
	@Lob
	public byte [] file;

	@OneToOne(mappedBy="photo")
	public Employee employee;

	public static Finder<Integer, Photo> find = new Finder<>(Photo.class);

	public Photo(String format,String name, Double size) {
		this.format = format;
		this.name = name;
		this.size = size;
	}

	public Photo(String format,String name, Double size, byte[] file) {
		this.format = format;
		this.name = name;
		this.size = size;
		this.file = file;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getSize() {
		return size;
	}

	public void setSize(Double size) {
		this.size = size;
	}



	public static Photo getById(int id) {
		return null;
	}


	public static ArrayList<Photo> getAll() {
		return null;
	}

	@Override
	public void insert() {
		this.save();
	}


	public void edit() {
		this.update();
	}


	public static void delete(int id) {

	}
}
