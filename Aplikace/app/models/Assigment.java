package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import io.ebean.Finder;
import io.ebean.Model;
import play.data.validation.Constraints;
import play.data.validation.ValidationError;

@Entity
@Table(name="assigment")
public class Assigment extends Model implements Constraints.Validatable<ArrayList<ValidationError>> {
	
	@Id
	public Integer assigmentId;
	public String title;
	public String description;
	
	@ManyToOne(optional=false)
	Project project;
	
	@OneToMany(mappedBy="assigment")
	public List<Task> tasks;

	@ManyToOne(optional=true)
	public Team team;

	public static Finder<Integer, Assigment> find = new Finder<>(Assigment.class);

	public Assigment(String title, String description, Project project, Team team) {
		this.title = title;
		this.description = description;
		this.project = project;
		this.team = team;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}



	public static Assigment getById(int id) {
		return null;
	}


	public static ArrayList getAll() {
		return null;
	}

	@Override
	public void insert() {

	}


	public void edit(int id, Assigment entity) {

	}


    public static void delete(int id) {

    }

	public Team getTeam() {
		return team;
	}

	public Project getProject() {
		return project;
	}

    public List<Task> getTasks() {
		return tasks;
	}

	public Integer getAssigmentId() {
		return assigmentId;
	}

	@Override
	public ArrayList<ValidationError> validate() {
		ArrayList<ValidationError> errors = new ArrayList<>();
		if (title.isEmpty())
			errors.add(new ValidationError("title", "Záznam nebyl přidán - chybí předmět úkolu"));

		if (!errors.isEmpty())
			return errors;

		return null;
	}
}
