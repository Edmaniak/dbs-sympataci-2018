package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import io.ebean.Finder;
import io.ebean.Model;

/**
 * 1 - admin
 * 2 - uzivatel
 * 3 - supervizor
 */
@Entity
@Table(name="role")
public class Role extends Model {
	
	@Id
	public Integer roleId;
	public String role;
	public String describtion;
	
	@OneToMany(mappedBy="role")
	public List<Employee> employees;

	public static Finder<Integer, Role> find = new Finder<>(Role.class);

	public Integer getRoleId() {
		return roleId;
	}


	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}


	public String getRole() {
		return role;
	}


	public void setRole(String role) {
		this.role = role;
	}


	public String getDescribtion() {
		return describtion;
	}


	public void setDescribtion(String describtion) {
		this.describtion = describtion;
	}



	public static Role getById(int id) {
		return null;
	}


	public static ArrayList<Role> getAll() {
		return null;
	}

	@Override
	public void insert() {

	}


	public void edit(int id, Role entity) {

	}


	public static void delete(int id) {

	}
}
