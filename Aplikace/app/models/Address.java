package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import io.ebean.Finder;
import io.ebean.Model;

@Entity
@Table(name="address")
public class Address extends Model {
	
	@Id
	public Integer addressId;
	public String street;
	public String citycode;
	public String town;
	public String telnumber;
	public String email;
	
	@OneToMany(mappedBy="address")
	public List<Employee> employees;
	
	@OneToOne(mappedBy = "address")
	public Customer customer;

	public static Finder<Integer, Address> find = new Finder<>(Address.class);

	public Address(String street, String citycode, String town, String telnumber, String email) {
		this.street = street;
		this.citycode = citycode;
		this.town = town;
		this.telnumber = telnumber;
		this.email = email;
	}


	public static Address getById(int id) {
		return null;
	}


	public static ArrayList<Address> getAll() {
		return null;
	}

	@Override
	public void insert() {

	}


	public void edit() {

	}


    public static void delete(int id) {

    }

	public String getEmail() {
		return email;
	}

	public String getTelnumber() {
		return telnumber;
	}

	public String getCitycode() {
		return citycode;
	}

	public String getStreet() {
		return street;
	}

	public String getTown() {
		return town;
	}

	public Integer getAddressId() {
		return addressId;
	}

	public void setAddressId(Integer addressId) {
		this.addressId = addressId;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public void setCitycode(String citycode) {
		this.citycode = citycode;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public void setTelnumber(String telnumber) {
		this.telnumber = telnumber;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public static void setFind(Finder<Integer, Address> find) {
		Address.find = find;
	}
}
