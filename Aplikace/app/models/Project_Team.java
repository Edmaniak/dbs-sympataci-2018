package models;

import io.ebean.Finder;
import io.ebean.Model;
import scala.Int;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "project_team")
public class Project_Team extends Model  {


    @Id
    public Integer id;

    public Integer projectId;
    public Integer teamId;

    public Project_Team(Integer projectId, Integer teamId) {
        this.projectId = projectId;
        this.teamId = teamId;
    }

    public static Finder<Integer, Project_Team> find = new Finder<>(Project_Team.class);

    public static Project_Team getById(int id) {
        return find.byId(id);
    }

    public static ArrayList<Project_Team> getAll() {
        return new ArrayList<>(find.all());
    }

    @Override
    public void insert() {
        this.save();
    }


    public void edit(int id, Project_Team entity) {

    }

    public static ArrayList<Team> getTeams(Integer projectId) {
        List<Project_Team> col = find.query().where().eq("project_id", projectId).findList();
        ArrayList<Team> returnedTeams = new ArrayList<>();
        for(Project_Team index : col)
            returnedTeams.add(Team.getById(index.teamId));
        return returnedTeams;
    }

    public static ArrayList<Project> getProjects(Integer teamId) {
        List<Project_Team> col = find.query().where().eq("team_id", teamId).findList();
        ArrayList<Project> projects = new ArrayList<>();
        for(Project_Team index : col)
            projects.add(Project.getById(index.projectId));
        return projects;
    }


    public static void delete(int id) {

    }
}
