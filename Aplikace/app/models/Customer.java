package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import io.ebean.Finder;
import io.ebean.Model;

@Entity
@Table(name="customer")
public class Customer extends Model {

	@Id
	public Integer customerId;
	public String name;
	
	@OneToOne
    public Address address;
	
	@OneToMany(mappedBy="customer")
	public List<Project> projects;

	public Customer(String name, Address address) {
		this.name = name;
		this.address = address;
	}

	public static Finder<Integer, Customer> find = new Finder<>(Customer.class);


	public static Customer getById(int id) {
		return null;
	}

	public static ArrayList<Customer> getAll() {
		return null;
	}

	@Override
	public void insert() {

	}


	public void edit() {

	}

    public static void delete(int id) {

    }

    public String getName() {
        return name;
    }

    public Address getAddress() {
        return address;
    }

    public Integer getCustomerId() {
        return customerId;
    }

	public List<Project> getProjects() {
		return projects;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}

}
