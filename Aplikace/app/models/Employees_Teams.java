package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import io.ebean.Finder;
import io.ebean.Model;

@Entity
@Table(name="employees_teams")
public class Employees_Teams extends Model {
	
	@Id
	public Integer employeesTeamsId;


	@ManyToOne(optional=true)
	Employee employee;
	@ManyToOne(optional=true)
	Team team;


	public static Finder<Integer, Employees_Teams> find = new Finder<>(Employees_Teams.class);

	public static Employees_Teams getById(int id) {
		return null;
	}


	public static ArrayList<Employees_Teams> getAll() {
		return null;
	}

	public Employees_Teams(Integer employeeEmployeeId, Integer teamTeamId) {
		this.employeeEmployeeId = employeeEmployeeId;
		this.teamTeamId = teamTeamId;
	}

	@Override
	public void insert() {
		this.save();
	}


	public void edit(int id, Employees_Teams entity) {

	}

	public static ArrayList<Employee> getEmployees(Integer teamId) {
		List<Employees_Teams> col = find.query().where().eq("team_team_id ", teamId).findList();
		ArrayList<Employee> returnedEmployees = new ArrayList<>();
		for(Employees_Teams index : col)
			returnedEmployees.add(Employee.getById(index.teamTeamId));
		return returnedEmployees;
	}

	public static ArrayList<Team> getTeams(Integer employeeId) {
		List<Employees_Teams> col = find.query().where().eq("employee_employee_id", employeeId).findList();
		ArrayList<Team> returnedTeams = new ArrayList<>();
		for(Employees_Teams index : col)
			returnedTeams.add(Team.getById(index.employeeEmployeeId));
		return returnedTeams;
	}


    public static void delete(int id) {

    }
}
