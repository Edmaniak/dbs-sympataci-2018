package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import io.ebean.Finder;
import io.ebean.Model;

@Entity
@Table(name = "team")
public class Team extends Model {

	@OneToMany(mappedBy="team")
	public List<Assigment> assigments;


	@OneToMany(mappedBy = "team", cascade = CascadeType.ALL)
	public List<Employees_Teams> Employees_Teams;

	public static Finder<Integer, Team> find = new Finder<>(Team.class);

	public Team(String name, Employee supervizor) {
		this.name = name;
		this.supervizor = supervizor;
	}

	public static Team getById(int id) {
		return null;
	}


	public static ArrayList<Team> getAll() {
		return null;
	}

	@Override
	public void insert() {

	}


    public void edit(int id, Team entity) {

	}


    public static void delete(int id) {
        find.deleteById(id);
        Employees_Teams.deleteTeam(id);
        Project_Team.deleteTeam(id);
    }


    public Employee getSupervizor() {
        return supervizor;
    }

    public String getName() {
        return name;
    }
    public ArrayList<Employee> getEmployees() {
    }

    public int getEmployeesCount() {
        return getEmployees().size() + 1;
    }

    public List<Project> getProjects() {
    }

    public List<Assigment> getAssigments() {
        return assigments;
    }

    public Integer getTeamId() {
        return teamId;
    }
}
