package models;

import java.util.ArrayList;
import java.util.List;

import io.ebean.Finder;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name = "employee")
public class Employee extends io.ebean.Model {

    @Id
    public Integer employeeId;
    

	public String firstname;
    public String lastname;
    public String password;
    public Double tax;
    
    /**
     * Constrains
     * 
     * */
    
    @ManyToOne(optional=true)
    public Address address;
    
    @ManyToOne(optional=true)
    public Role role;
    
    @OneToMany(mappedBy="employee")
    public List<Task> tasks;

	@OneToMany(mappedBy = "employee")
	public List<Employees_Teams> Employees_Teams;

  
    
    @OneToOne
    Photo photo;

	public Employee(String firstname, String lastname, String password, Double tax) {
		this.firstname = firstname;
		this.lastname = lastname;
		this.password = password;
		this.tax = tax;
	}

    public Employee(String firstname, String lastname, String password, Double tax, Address address, Role role, Photo photo) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.password = password;
        this.tax = tax;
        this.address = address;
        this.role = role;
        this.photo = photo;
    }

	public static Finder<Integer, Employee> find = new Finder<>(Employee.class);

    public Employee(String firstname, String lastname, Double tax, Address address, Role role, Photo photo) {
        this(firstname, lastname, "", tax, address, role, photo);
    }

    public Employee(Integer employeeId, String firstname, String lastname, Double tax, Address address) {
        this(employeeId, firstname, lastname, "", tax, address);
    }

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	public Double getTax() {
		return tax;
	}

	public void setTax(Double tax) {
		this.tax = tax;
	}


	public static Employee getById(int id) {
		return null;
	}


	public static ArrayList<Employee> getAll() {
		return null;
	}

	@Override
	public void insert() {

	}
    public Role getRole() {
        return role;
    }

	@Override
	public void edit(int id, Employee entity) {


    public void edit() {
        this.update();
    }

    public static void delete(int id) {
        find.deleteById(id);
    }

    public ArrayList<Project> getAllProjects() {
        ArrayList<Team> teams = getAllTeams();
        ArrayList<Project> allProjects = new ArrayList<>();
        for (Team team : teams) {
            List<Project> projects = team.getProjects();
            for (Project project : projects)
                allProjects.add(project);
        }
        return allProjects;
    }

    public ArrayList<Assigment> getAllAssigments() {
        ArrayList<Assigment> assigments = new ArrayList<>();
        for (Project project : getAllProjects()) {
            List<Assigment> a = project.getAssigment();
            for (Assigment ass : a) {
                assigments.add(ass);
            }
        }
        return assigments;
    }
    public ArrayList<Task> getAllTasks() {
    }
    public ArrayList<Team> getAllTeams() {
        ArrayList<Team> teams = new ArrayList<>();
        teams.add(team);
        teams.addAll(Employees_Teams.getTeams(employeeId));
        return teams;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public ArrayList<Task> getTasksOfAssigment(Integer assigmentId) {
        ArrayList<Task> tasks = new ArrayList<>();
        for (Task task : getTasks())
            if (task.getAssigment().getAssigmentId() == assigmentId)
                tasks.add(task);
        return tasks;
    }

    public static Employee getByEmail(String email) {
        return find.query().where().eq("address.email", email).findOne();
    }

    public static boolean isValid(String email, String password) {
        Employee employee = getByEmail(email);
        return employee != null
                && password != null
                && employee.getPassword().equals(password);
    }

    public ArrayList<Project> getAllSupervisedProjects() {
        ArrayList<Project> projects = new ArrayList<>();

        for (Team t : getAllTeams()) {
            if (t.getSupervizor().getEmployeeId() == getEmployeeId()) {
                projects.addAll(t.getProjects());
            }
        }

        return projects;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public Photo getPhoto() {
        return photo;
    }

    public Team getTeam() {
        return team;
    }

    @Override
    public ArrayList<ValidationError> validate() {
        ArrayList<ValidationError> errors = new ArrayList<>();
        if (firstname.isEmpty() || lastname.isEmpty())
            errors.add(new ValidationError("name", "Není vyplněno jméno"));
        if(password.isEmpty() || password.length() < 6)
            errors.add(new ValidationError("passowrd", "Heslo má nedostatečný počet znaků - minimum je 6 znaků"));
        if (errors.isEmpty())
            return null;
        return errors;
    }
}
