package models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import io.ebean.Finder;
import io.ebean.Model;
import play.data.validation.Constraints;
import play.data.validation.ValidationError;

import java.util.ArrayList;

@Entity
@Table(name = "cost")
public class Cost extends Model implements Constraints.Validatable<ArrayList<ValidationError>> {

    @Id
    public Integer costId;
    public Integer cost;
    public String name;
    public String describtion;

    @ManyToOne(optional = false)
    public Project project;

    public static Finder<Integer, Cost> find = new Finder<>(Cost.class);

    public Cost(String name, String describtion, Integer cost, Project project) {
        this.name = name;
        this.describtion = describtion;
        this.cost = cost;
        this.project = project;
    }


    public static Cost getById(int id) {
        return null;
    }


    public static ArrayList<Cost> getAll() {
        return null;
    }

    @Override
    public void insert() {

    }


    public void edit(int id, Cost entity) {

    }


    public static void delete(int id) {

    }

    public static Integer getAllCosts() {
        Integer amount = 0;
        for(Cost cost : getAll()) {
            amount += cost.getCost();
        }
        return amount;
    }
    

    public String getName() {
        return name;
    }

    public Project getProject() {
        return project;
    }

    public Integer getCostId() {
        return costId;
    }

    public String getDescribtion() {
        return describtion;
    }

    public Integer getCost() {
        return cost;
    }

    @Override
    public ArrayList<ValidationError> validate() {
        ArrayList<ValidationError> errors = new ArrayList<>();
        if (name.isEmpty())
            errors.add(new ValidationError("name", "Záznam nebyl přidán - není vyplněno jméno"));
        if (errors.isEmpty())
            return null;
        return errors;
    }
}
