package models;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import io.ebean.Finder;
import io.ebean.Model;
import io.ebeaninternal.server.lib.util.Str;
import play.data.validation.Constraints;
import play.data.validation.ValidationError;

@Entity
@Table(name = "task")
public class Task extends Model implements Constraints.Validatable<ArrayList<ValidationError>> {

    @Id
    public Integer taskId;
    public Date date;
    @Column(length = 11)
    public Integer hours;
    @Column(length = 40)
    public String subject;
    public String description;

    @ManyToOne(optional = false)
    public Assigment assigment;

    @ManyToOne(optional = false)
    Employee employee;

	public static Task getById(int id) {
		return find.byId(id);
	}

    public Task(LocalDate date, Integer hours, String subject, String description, Assigment assigment, Employee employee) {
        this.date = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
        this.hours = hours;
        this.subject = subject;
        this.description = description;
        this.assigment = assigment;
        this.employee = employee;
    }

	@Override
	public void insert() {
		this.save();
	}

    public static Task getById(int id) {
        return find.byId(id);
    }


    public static ArrayList<Task> getAll() {
        return new ArrayList<>(find.all());
    }

    @Override
    public void insert() {
        this.save();
    }


    public void edit(int id, Task entity) {

    }

    public static void delete(Integer id) {
        find.deleteById(id);
    }

    public Employee getEmployee() {
        return employee;
    }

    public Assigment getAssigment() {
        return assigment;
    }

    public static void delete(int id) {

    }

    public Integer getHours() {
        return hours;
    }

    public String getDate() {
        SimpleDateFormat f =  new SimpleDateFormat("dd-MM-yyyy");
        return f.format(date);
    }

    @Override
    public ArrayList<ValidationError> validate() {
        ArrayList<ValidationError> errors = new ArrayList<>();
        if (subject.isEmpty())
            errors.add(new ValidationError("subject", "Záznam nebyl přidán - chybí předmět práce"));

        if (!errors.isEmpty())
            return errors;

        return null;
    }
}
