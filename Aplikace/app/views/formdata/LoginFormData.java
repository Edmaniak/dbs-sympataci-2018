package views.formdata;

import models.Employee;
import play.data.validation.Constraints;
import play.data.validation.ValidationError;

import javax.validation.Constraint;
import java.util.ArrayList;
import java.util.List;

public class LoginFormData implements Constraints.Validatable<String> {

    @Constraints.Required
    public String email;
    @Constraints.Required
    public String password;

    public LoginFormData() {

    }

    @Override
    public String validate() {
        List<ValidationError> errors = new ArrayList<>();
        if (Employee.isValid(email, password)) {
            return null;
        }
        return "Špatné přihlašovací údaje";
    }

}
