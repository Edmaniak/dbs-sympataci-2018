var gulp = require('gulp');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var concat = require('gulp-concat');

gulp.task('style', function() {
    gulp.src('resources/sass/main.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('public/stylesheets/'));
    gulp.src('resources/css/**/*.css')
        .pipe(concat('main.css'));
});

gulp.task('js', function() {
    gulp.src('resources/js/**/*.js')
        .pipe(concat('script.js'))
        .pipe(uglify())
        .pipe(gulp.dest('public/javascript/'));
});



gulp.task('watch',function() {
    gulp.watch('resources/sass/**/*.scss',['style']);
    gulp.watch('resources/js/**/*.js',['js']);
});