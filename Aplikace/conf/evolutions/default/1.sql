# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table address (
  address_id                    integer auto_increment not null,
  street                        varchar(40),
  citycode                      varchar(40),
  town                          varchar(40),
  telnumber                     varchar(40),
  email                         varchar(40),
  constraint pk_address primary key (address_id)
);

create table assigment (
  assigment_id                  integer auto_increment not null,
  title                         varchar(40),
  description                   varchar(255),
  project_project_id            integer not null,
  team_team_id                  integer,
  constraint pk_assigment primary key (assigment_id)
);

create table cost (
  cost_id                       integer auto_increment not null,
  cost                          integer(11),
  name                          varchar(40),
  describtion                   varchar(255),
  project_project_id            integer not null,
  constraint pk_cost primary key (cost_id)
);

create table customer (
  customer_id                   integer auto_increment not null,
  name                          varchar(40),
  address_address_id            integer,
  constraint uq_customer_address_address_id unique (address_address_id),
  constraint pk_customer primary key (customer_id)
);

create table employee (
  employee_id                   integer auto_increment not null,
  firstname                     varchar(40),
  lastname                      varchar(40),
  password                      varchar(255),
  tax                           double,
  login                         varchar(40),
  address_address_id            integer,
  role_role_id                  integer,
  photo_photo_id                integer,
  constraint uq_employee_photo_photo_id unique (photo_photo_id),
  constraint pk_employee primary key (employee_id)
);

create table employees_teams (
  employees_teams_id            integer auto_increment not null,
  employee_employee_id          integer,
  team_team_id                  integer,
  constraint pk_employees_teams primary key (employees_teams_id)
);

create table photo (
  photo_id                      integer auto_increment not null,
  format                        varchar(40),
  name                          varchar(40),
  size                          double,
  file                          longblob,
  constraint pk_photo primary key (photo_id)
);

create table project (
  project_id                    integer auto_increment not null,
  name                          varchar(40),
  budget                        integer(11),
  dead_line                     datetime(6),
  state                         tinyint(1) default 0,
  customer_customer_id          integer,
  constraint pk_project primary key (project_id)
);

create table project_team (
  id                            integer auto_increment not null,
  project_id                    integer,
  team_id                       integer,
  constraint pk_project_team primary key (id)
);

create table role (
  role_id                       integer auto_increment not null,
  role                          varchar(40),
  describtion                   varchar(255),
  constraint pk_role primary key (role_id)
);

create table task (
  task_id                       integer auto_increment not null,
  date                          datetime(6),
  hours                         integer,
  subject                       varchar(255),
  description                   varchar(255),
  assigment_assigment_id        integer not null,
  employee_employee_id          integer not null,
  constraint pk_task primary key (task_id)
);

create table team (
  team_id                       integer auto_increment not null,
  name                          varchar(40),
  supervizor_employee_id        integer,
  constraint uq_team_supervizor_employee_id unique (supervizor_employee_id),
  constraint pk_team primary key (team_id)
);

alter table assigment add constraint fk_assigment_project_project_id foreign key (project_project_id) references project (project_id) on delete restrict on update restrict;
create index ix_assigment_project_project_id on assigment (project_project_id);

alter table assigment add constraint fk_assigment_team_team_id foreign key (team_team_id) references team (team_id) on delete restrict on update restrict;
create index ix_assigment_team_team_id on assigment (team_team_id);

alter table cost add constraint fk_cost_project_project_id foreign key (project_project_id) references project (project_id) on delete restrict on update restrict;
create index ix_cost_project_project_id on cost (project_project_id);

alter table customer add constraint fk_customer_address_address_id foreign key (address_address_id) references address (address_id) on delete restrict on update restrict;

alter table employee add constraint fk_employee_address_address_id foreign key (address_address_id) references address (address_id) on delete restrict on update restrict;
create index ix_employee_address_address_id on employee (address_address_id);

alter table employee add constraint fk_employee_role_role_id foreign key (role_role_id) references role (role_id) on delete restrict on update restrict;
create index ix_employee_role_role_id on employee (role_role_id);

alter table employee add constraint fk_employee_photo_photo_id foreign key (photo_photo_id) references photo (photo_id) on delete restrict on update restrict;

alter table project add constraint fk_project_customer_customer_id foreign key (customer_customer_id) references customer (customer_id) on delete restrict on update restrict;
create index ix_project_customer_customer_id on project (customer_customer_id);

alter table task add constraint fk_task_assigment_assigment_id foreign key (assigment_assigment_id) references assigment (assigment_id) on delete restrict on update restrict;
create index ix_task_assigment_assigment_id on task (assigment_assigment_id);

alter table task add constraint fk_task_employee_employee_id foreign key (employee_employee_id) references employee (employee_id) on delete restrict on update restrict;
create index ix_task_employee_employee_id on task (employee_employee_id);

alter table team add constraint fk_team_supervizor_employee_id foreign key (supervizor_employee_id) references employee (employee_id) on delete restrict on update restrict;


# --- !Downs

alter table assigment drop foreign key fk_assigment_project_project_id;
drop index ix_assigment_project_project_id on assigment;

alter table assigment drop foreign key fk_assigment_team_team_id;
drop index ix_assigment_team_team_id on assigment;

alter table cost drop foreign key fk_cost_project_project_id;
drop index ix_cost_project_project_id on cost;

alter table customer drop foreign key fk_customer_address_address_id;

alter table employee drop foreign key fk_employee_address_address_id;
drop index ix_employee_address_address_id on employee;

alter table employee drop foreign key fk_employee_role_role_id;
drop index ix_employee_role_role_id on employee;

alter table employee drop foreign key fk_employee_photo_photo_id;

alter table project drop foreign key fk_project_customer_customer_id;
drop index ix_project_customer_customer_id on project;

alter table task drop foreign key fk_task_assigment_assigment_id;
drop index ix_task_assigment_assigment_id on task;

alter table task drop foreign key fk_task_employee_employee_id;
drop index ix_task_employee_employee_id on task;

alter table team drop foreign key fk_team_supervizor_employee_id;

drop table if exists address;

drop table if exists assigment;

drop table if exists cost;

drop table if exists customer;

drop table if exists employee;

drop table if exists employees_teams;

drop table if exists photo;

drop table if exists project;

drop table if exists project_team;

drop table if exists role;

drop table if exists task;

drop table if exists team;

